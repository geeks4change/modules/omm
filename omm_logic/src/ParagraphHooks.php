<?php

namespace Drupal\omm_logic;

use Drupal\datetime\Plugin\Field\FieldType\DateTimeFieldItemList;
use Drupal\entitytools\DateFieldHelper;
use Drupal\entitytools\EntityNestedProperty;
use Drupal\entitytools\EntityOriginalNestedProperty;
use Drupal\paragraphs\ParagraphInterface;

class ParagraphHooks {

  public static function onPreSaveSetEntranceExit(ParagraphInterface $entity, $workflowField, $entranceDateField, $exitDateField) {
    $currentState = EntityNestedProperty::create($entity)->getNestedValue("$workflowField/0/value");
    $originalState = EntityOriginalNestedProperty::create($entity)->getNestedValue("$workflowField/0/value");

    $entranceDate = EntityNestedProperty::create($entity)->getNestedObject("$entranceDateField");
    if ($entranceDate instanceof DateTimeFieldItemList) {
      if (OmmFieldInfo::isMemberState($currentState) && !OmmFieldInfo::isMemberState($originalState)) {
        DateFieldHelper::set($entranceDate);
      }
    }

    $exitDate = EntityNestedProperty::create($entity)->getNestedObject("$exitDateField");
    if ($exitDate instanceof DateTimeFieldItemList) {
      if (OmmFieldInfo::isMemberState($currentState)) {
        DateFieldHelper::clear($exitDate);
      }
      elseif (!OmmFieldInfo::isMemberState($currentState) && OmmFieldInfo::isMemberState($originalState)) {
        DateFieldHelper::set($exitDate);
      }
    }

  }

}
