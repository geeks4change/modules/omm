<?php

namespace Drupal\omm_logic;

use Drupal\entitytools\EntityNestedProperty;
use Drupal\user\Entity\User;

class Callbacks {

  /**
   * Migration callback to get paying user.
   *
   * @param $uid
   *   The uid of the buying user.
   * @return array
   *   The paying user as array.
   */
  public static function payingUser($uid) {
    $user = User::load($uid);
    $payingUserPropertyPath = implode('/', [
      'field_omm_membership_interested/0/entity',
      'field_omm_membership_data/0/entity',
      'field_omm_membership_payment/0/entity',
      'field_omm_paying_member/0/entity',
    ]);
    /** @var \Drupal\user\UserInterface|null $payingUser */
    $payingUser = (new EntityNestedProperty($user))->getNestedValue($payingUserPropertyPath);
    if ($payingUser) {
      return $payingUser->toArray();
    }
    elseif ($user) {
      return $user->toArray();
    }
    else {
      // The user may have been deleted, EntityNestedProperty eats this.
      return [];
    }
  }

}
