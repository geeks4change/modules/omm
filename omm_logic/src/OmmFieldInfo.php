<?php

namespace Drupal\omm_logic;

class OmmFieldInfo {

  public static function isMemberState($state) {
    return in_array($state, ['member','buyer_access']);
  }

}
