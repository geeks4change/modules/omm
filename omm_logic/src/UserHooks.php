<?php

namespace Drupal\omm_logic;

use Drupal\entitytools\EntityNestedProperty;
use Drupal\entitytools\EntityOriginalNestedProperty;
use Drupal\omm_logic\MessageCreator;
use Drupal\user\UserInterface;

class UserHooks {

  public static function onPostSaveCreateAmountChangedMessages(UserInterface $user, $path, $fieldNames) {
    foreach ($fieldNames as $fieldName => $accountType) {
      $currentAmount = EntityNestedProperty::create($user)->getNestedValue("$path/$fieldName/0/value", 0);
      $originalAmount = EntityOriginalNestedProperty::create($user)->getNestedValue("$path/$fieldName/0/value", 0);
      $amount = $currentAmount - $originalAmount;
      if ($amount) {
        MessageCreator::create($user, $amount, $accountType);
      }
    }
  }

  public static function onPreSaveAdjustMemberRole(UserInterface $user) {
    $path = 'field_omm_membership_interested/0/entity/field_omm_membership/0/value';
    $state = EntityNestedProperty::create($user)->getNestedValue($path);

    if (in_array($state, ['member', 'buyer_access'])) {
      $user->addRole('omm_member');
    }
    else {
      $user->removeRole('omm_member');
    }

    if (in_array($state, ['buyer_access'])) {
      $user->addRole('omm_buyer');
    }
    else {
      $user->removeRole('omm_buyer');
    }
  }

}
