<?php

namespace Drupal\omm_logic;

use Drupal\message\Entity\Message;
use Drupal\user\UserInterface;

class MessageCreator {

  public static function create(UserInterface $user, $amount, $account) {
    $message = Message::create([
      'template' => 'omm_balance_change',
      'uid' => $user->id(),
      'field_omm_amount' => $amount,
      'field_omm_account' => $account,
    ]);
    $message->save();
  }

}
